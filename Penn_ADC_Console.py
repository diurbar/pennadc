"""
Created Jan 19 2017 
Annotated Mar 9 2017

Author: Richard Diurba, Advisor: Rick Van Berg, PI: Professor Josh Klein
"""
#Dependecies
import sys
import glob
import os
import numpy as np
import matplotlib.pyplot as plt
sys.path.append("/home/hep/DUNE/ADC_Data_Analysis/scripts/modules/")
import Penn_ADCData as adcdata
import Penn_ADCData_rootlin as rootlin
import Penn_ADCSettings as read_settings
directory=sys.argv[1] #Argument that gives the directory repo which we loop through
start_hist=2000000 #Starting point for the general histograms
baseline=5000000 #Starting point near the baseline
minimums=[]
def main(directory):
    directory=str(directory) #Directory of the data points
    directory, path=read_settings.get_directory(directory)
    #print directory, path
    for filename in glob.glob(path): #Loop through data repo
        filenamechip, filenamechannel=read_settings.get_file_info(directory, filename)
        #print directory, filename
        if directory=='../data_17_03/data_repo_17_03/' or directory=='../data_17_06/data_repo_17_06/' or directory=='../data_17_07/data_repo_17_07/':
            data=np.array(adcdata.bin_pull(filename, filenamechannel, directory), dtype=object)
            if data[2]>10000000:
                minimums.append(data[2])
        else:
	    data=np.array(adcdata.csv_pull(filename, directory), dtype=object) #Pulls data
        #adcdata.hist_general(filename, data[0], data[1], start_hist, filenamechip, filenamechannel) 
        #Creates histogram at 2 million increments of 1k data arrays
        adcdata.std_checker(filename, data[1], data[0], filenamechip, filenamechannel, directory) #Calculates std. of 1k data arrays
	#adcdata.chi2_checker(filename, data[1], data[0], filenamechip, filenamechannel, directory)
	#adcdata.linreg(filename, data[0], data[1], filenamechip, filenamechannel, directory) #Calculate linear regression
	#adcdata.hist_baseline(filename, data[0], data[1], baseline, filenamechip, filenamechannel) #Calculates histogram near baseline
     #adcdata.hist_avg_baseline(filename, data[0], data[1], baseline, filenamechip, filenamechannel) #Calculates histogram near baseline
	#adcdata.hist_random_baseline(filename, data[0], data[1], baseline, filenamechip, filenamechannel) #Calculates random histogram near baseline
	#rootlin.lin_fit(filename, data[0], data[1], filenamechip, filenamechannel, directory)
	#adcdata.residual_cali(filename, data[0], data[1], filenamechip, filenamechannel)
	#adcdata.linear_cali(data[0], data[1], filename, filenamechip, filenamechannel)
	adcdata.scatter(filename, data[0], data[1], filenamechip, filenamechannel) #Graphs basic scatter plot	
        hists=adcdata.histogram_dynamic(filename, data[0], data[1], filenamechip, filenamechannel, directory) #Graphs basic scatter plot	
        dev.linfit_hist(filename, hists[0], hists[1], hists[2], hists[3], filenamechip, filenamechannel, directory)
        #dev.linfit_hist_inv(filename, hists[0], hists[1], hists[2], hists[3], filenamechip, filenamechannel, directory)
        adcdata.calc_inl_dnl(filename, hists[0], hists[1], filenamechip, filenamechannel, directory)
     #adcdata.hist_resi(filename, hists[0], hists[1], filenamechip, filenamechannel, directory)
     #print np.mean(minimums), data[2]
main(directory)

