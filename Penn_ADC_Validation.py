from ROOT import TFile
import matplotlib.pyplot as plt
import numpy as np
import sys
import matplotlib
matplotlib.rcParams.update({'font.size':6})
chip=sys.argv[1]
def main():
    gen_DNL_plots(chip)
    gen_RMS_plots(chip)
    #gen_lin_plots(chip)
    
def gen_DNL_plots(chip):
    dnl=[]
    inl=[]
    voltage=[]
    counts=[]
    f=TFile('../ROOT_repository/Merged_ADC_ROOT/'+chip+'_LN_merged_DNL_INL.root')
    tree=f.Get("DNL_INL")
    for entry in tree:
        if abs(entry.dnl)<40:
            dnl.append(entry.dnl)
            inl.append(entry.inl)
            voltage.append(entry.voltage)
            counts.append(entry.count)
    filename=('../ROOT_repository/Merged_Graphs/'+chip+'_LN_merged_DNL_INL.jpg')
    dnl_inl=plt.figure()
    plt.subplot(211)
    plt.plot(voltage, dnl) 
    plt.xlabel('Voltage of Pulser (uV)')
    plt.ylabel('DNL (counts)')
    plt.title('Plot of DNL for %s in LN over Multiple Datasets ($\mu$=%s, $\sigma$=%s)'%(chip, round(np.mean(dnl),4), round(np.std(dnl), 4)))
    plt.xlim(500000, 1400000)
    plt.ylim(-10, 10)
    plt.subplot(212)
    plt.plot(voltage, inl)
    plt.xlabel('Voltage of Pulser (uV)')
    plt.ylabel('INL (counts)')
    plt.xlim(500000, 1400000)
    plt.ylim(0, 200)
    plt.title('Plot of INL for %s in LN over Multiple Datasets ($\mu$=%s, $\sigma$=%s)'%(chip, round(np.mean(inl),4), round(np.std(inl), 4))) 
    plt.savefig(filename) #Save the image
    plt.close()    #Close pyplot
def gen_RMS_plots(chip):
    rms=[]
    voltage=[]
    counts=[]
    f=TFile('../ROOT_repository/Merged_ADC_ROOT/'+chip+'_LN_merged_rms.root')
    tree=f.Get("RMS")
    for entry in tree:
	rms.append(entry.rms)
        voltage.append(entry.voltage)
        counts.append(entry.count)
    filename=('../ROOT_repository/Merged_Graphs/'+chip+'_LN_merged_rms.jpg')
    rms_plot=plt.figure()
    plt.plot(voltage, rms) 
    plt.xlabel('Voltage of Pulser (uV)')
    plt.ylabel('RMS (counts)')
    plt.title('Plot of RMS for %s in LN over Multiple Datasets ($\mu$=%s, $\sigma$=%s)'%(chip, round(np.mean(rms),4), round(np.std(rms), 4)))
    #plt.xlim(500000, 1400000)
    #plt.ylim(0, 10)
    plt.savefig(filename) #Save the image
    plt.close()    #Close pyplot


main()
    
    



