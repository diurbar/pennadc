#README#

Cold ADC Analysis Scripts

Developed by Richard Diurba (Penn) for Deep Underground Neutrino Experiment (DUNE)

#Purpose#

Cold ADC Analysis is a collection of Python scripts intended to be used over a repository of ADC data originally intended for analyzing the Brookhaven National Laboratory (BNL) cold ADC developed for the Deep Underground Neutrino Experiment (DUNE).

#Outline#

Scripts determine the linearity, differential non-linearity, chi-squared, RMS, and long term calibration capability of a chip initially intended for ADCs.

#Software Packages Required#

Python 2.7.9

ROOT 6.04.18

scipy 0.14.0

numpy 1.11.0

matplotlib 1.4.2


#Script Documentation#

The scripts intend to use a wrapper and run scripting tests over a directory of data files. The wrapper takes two imported scripts where the tests are developed and written in Python and Pyroot. If one wishes to run the tests with the wrapper, the argument needs to point to the directory of data files ('../directory/to data files/').

To run the scripts run the following:

python Penn_ADC_Console.py data_file_directory