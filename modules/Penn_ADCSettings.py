"""
Created Aug 4 2017 
Annotated Aug 4 2017

Author: Richard Diurba, Advisor: Rick Van Berg, PI: Professor Josh Klein
"""
def get_directory(repo):
    directory="../data/"+repo+"/"+repo[0:4]+"_repo_"+repo[5:]+"/"
    path=directory+'/*.csv'
    if directory=='../data/data_17_03/data_repo_17_03/' or directory=='../data/data_17_06/data_repo_17_06/' or directory=='../data/data_17_07/data_repo_17_07/':
        path=directory+'/*.bin'
    return directory, path
def get_file_info(directory, filename):
    	if directory=='../data/data_16_11/data_repo_16_11/': 
    	    #These if statements determine where the chip and channel is to properly 
    	    filenamechannel=filename[71]
    	    filenamechip=filename[43:47]
        elif directory=='../data/data_17_1_1MHz/data_repo_17_1/':
    	    filenamechannel=filename[58]
            filenamechip=filename[38:40]+filename[43:46]
        elif directory=='../data/data_17_01/data_repo_17_01/':
    	    filenamechannel=filename[55]
            filenamechip=filename[35:37]+filename[40:43]
        elif directory=='../data/data_17_02/data_repo_17_02/':
    	    filenamechannel=filename[55]
            filenamechip=filename[35:37]+filename[40:43]
        elif directory=='../data/data_LTCADC/data_repo_LTCADC/':
    	    filenamechannel=filename[39]
            filenamechip=filename[46:53]
        elif directory=='../data/data_cots/data_repo_cots/':
    	    filenamechannel=filename[58]
            filenamechip=filename[33:40]
            if len(filename)==66:
    	        filenamechannel=filename[57]
                filenamechip=filename[33:39]
        elif directory=='../data/data_17_03/data_repo_17_03/' or directory=='../data/data_17_03/data_rep2_17_03/':
    	    filenamechannel=filename[58]
    	    if len(filename)>75:
    	        filenamechannel=filename[59]
            filenamechip=filename[35:37]+filename[40:43]
        elif directory=='../data/data_17_06/data_repo_17_06/':
            filenamechannel=filename[56]
            filenamechip=filename[35:37]+filename[40:44]
            if filename[42]=='_':
               filenamechip=filename[35:37]+filename[41]
        elif directory=='../data/data_17_07/data_repo_17_07/':
            if len(filename)==75:
                filenamechip=filename[35:37]+filename[40:44]
                filenamechannel=filename[58]
            elif len(filename)==64:
                filenamechip=filename[35:37]+filename[40:43]
                filenamechannel=filename[57]
            elif len(filename)==73:
                filenamechip=filename[35:37]+filename[40:42]
                filenamechannel=filename[56]
            print len(filename), filenamechip, filenamechannel
        return filenamechip, filenamechannel
def get_linparameters(x, directory):
    if directory=='../data/data_cots/data_repo_cots/':
        start=1500000
        length=len(x)-1500000
    elif directory=='../data/data_17_06/data_repo_17_06/' or directory=='../data/data_17_07/data_repo_17_07/':
        start=2500000
        length=len(x)-3000000
    else:
        start=5000000 
        length=len(x)-6000000 
    return start, length
