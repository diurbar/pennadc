# -*- coding: utf-8 -*-
"""
Created on Thu Jan 12 19:23:12 2017
Annotated on Thu Mar 9 12:58:05 2017

Author: Richard Diurba, Advisor: Rick Van Berg, PI: Professor Josh Klein
"""
# Dependencies Required 
import sys
import os
import binascii
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import scipy.stats as stats
import matplotlib
import scipy
import ROOT
from scipy.optimize import curve_fit
import Penn_ADCSettings as read_settings
rand=random.SystemRandom()
matplotlib.rcParams.update({'font.size':6}) # Fix text size of the plot titles, etc.
def bin_pull(filename, filenamechannel, directory):
    with open(filename, 'rb') as f:
        hexdata=binascii.hexlify(f.read())
    data=[]
    minimum=[]
    maximum=[]
    i=0
    while i<len(hexdata):
        data_word=hexdata[i+3]+hexdata[i]+hexdata[i+1]
        data_word=int(data_word, 16)
        data.append(data_word)
        i=i+4
    i=1
    while i<(len(data)-5):
        if data[(i-3)]<=5 and data[(i-2)]<=5 and data[(i-1)]<=5 and data[i]<=5 and data[(i+1)]<=5 and data[(i+2)]<=5 and data[(i+3)]<=5:
            minimum.append(i)
        if data[(i-3)]>=4090 and data[(i-2)]>=4090 and data[(i-1)]>=4090 and data[i]>=4090 and data[(i+1)]>=4090 and data[(i+2)]>=4090 and data[(i+3)]>=4090:
            maximum.append(i)
        i=i+1
    length=len(data)/2
    trough=10095959
    if np.mean(minimum)>10000000:
        #if np.mean(maximum)>10000000:
        trough=int((minimum[-1]+minimum[0])/2)
        reader=data[trough:(trough+20000000)]
        #peak=int(np.mean(maximum))
        #reader=data[(peak-20000000):peak]
    elif directory=='../data_17_07/data_repo_17_07/':
    	reader=data[0:20000000]
    else:
        reader=data[trough:(trough+20000000)]
    x=len(reader) #Create x-axis, BNL data contains just the y-data 
    #print len(data[trough:peak])
    voltage=x*.1  #From the calibration data from 11/26 each input point represents 100 nV. Line converts to uV.
    x=np.linspace(-300000,voltage-300000, x) #Using the voltage conversion and the length of the y-axis, create x-axis
    reader=[x,reader, trough] #Create 2-D array from the two axes
    #print np.mean(peak)
    #print len(reader[1]),  reader[1][0]
    if directory=='../data_17_06/data_repo_17_06/' or directory=='../data_17_07/data_repo_17_07/':
        data=data[5000000:14000000]
        x=len(data)
        voltage=x*.1
        x=np.linspace(-300000, 1700000, x)
        reader=[x, data, trough]
    return reader
def csv_pull(filename, directory): #Pull the data from the cold ADC BNL DUNE testing files
    reader = np.genfromtxt(filename, delimiter=',')
    x=len(reader) #Create x-axis, BNL data contains just the y-data 
    voltage=x*.1  #From the calibration data from 11/26 each input point represents 100 nV. Line converts to uV.
    #x=np.linspace(-300000,voltage-300000, x) #Using the voltage conversion and the length of the y-axis, create x-axis
    #print voltage
    if directory=='../data_cots/data_repo_cots/' and filename[28:34]=='ad7274':
        x=np.linspace(-100000, 1900000, x)
        print filename[28:34]
    elif directory=='../data_cots/data_repo_cots/' and filename[28:34]!='ad7274':
        x=np.linspace(-100000, 2600000, x)
        print filename[28:34]
    else:
        x=np.linspace(-300000,voltage-300000, x)
    reader=[x,reader] #Create 2-D array from the two axes
    #print len(reader[1])
    return reader #Return array
def hist_general(filename, x, y, start, filenamechip, filenamechannel): #Creates histogram at 2 million increment lengths of 1k datapoints
    test=[start, 1000+start, start+2000000, start+2001000, start+4000000,start+4001000, start+6000000, start+6001000] 
    #Creates array to index the x-axis
    i=0 #Variables for the loop of subplots that will be created for each histogram
    j=0 #Variables j and m loop over the points in the array, i counts what plot we are on
    m=1
    hist_plots=plt.figure() # Create the figure and give it a title below
    hist_plots.suptitle('Histogram of %s Chn:%s ADC At 2 million count increments'%(filenamechip, filenamechannel), fontsize=12)
    while i<4: # Loop to make subplots
    	plot_no=221+i #Subplot number
    	plt.subplot(plot_no) #Make subplot
    	plt.hist(y[test[j]:test[m]]) #Make subplot histogram
    	std=np.std(y[test[j]:test[m]]) #Take std. of histogram
    	#kurt=stats.kurtosis(y[test[j]:test[m]]) #Take kurtosis of histogram
    	plt.ylim(0, 600) #Limit the graph so each graph has an equal y-axis
    	plt.ylabel("Frequency") #Y-axis is the number of ADC counts outputted by the ADC
    	plt.title('Hist. for N=[%s:%s]'%(test[j],test[m])) #Title with indecies of the histogram
    	plt.xlabel("ADC Output (s=%s)"%(std)) #X-axis is the voltage input, labelled with std. and kurtosis
    	i=i+1 #Go through the loop again until all four graphs are made
    	j=j+2 #We only want each 1k sample at every two million points so we move two indexes from our current position
    	m=m+2
    filesave=(filename+'_hist_compar.jpg') #Give image name
    plt.savefig(filesave) #Save image
    plt.close() #Close pyplot
def std_checker(filename, y, x, filenamechip, filenamechannel, directory): #Calculate approximate HWHM of 1k histogram points
    start, length=read_settings.get_linparameters(x, directory)
    x=x[start:length]
    y=y[start:length]    
    b=(len(y)/1000)-1  #Find the limit of the number of 1k point arrays
    i=0 #Variable for the loop to calculate all HWHM for the data file. 
    std_array=[] #These are the arrays that we will plot and store
    std_bad=[]
    interval=[]
    count_avg=[]
    while i<b: #Loop to calculate the HWHM
    	j=1000*i #Indecies for the 1k data arrays
    	k=1000+j #End index for each 1k data array
    	std=np.std(y[j:k]) #Caculate HWHM
    	average=np.mean(y[j:k])
        count_avg.append(average)           
    	#rms=np.sqrt(np.mean(np.square(y[j:k]))) #Calculate RMS
    	interval.append(np.mean(x[j:k])) #Append as the interval of the 1k data array (x-axis)
    	std_array.append(std) #Append as the std. of the interval (y-axis)
    	if std>5: #If the std. is above 5 (an arbitrary cutoff), we store the data in a csv file if the user wants to see it
    	    std_bad.append(std)
    	    if x[k]>1000000: 
    	    #There are high std. below 1 million points, so it is good to only store unusual std. so we cutoff what bad data points are stored
    	        with open('bad_std.csv', 'a') as csvfile:
    	            csvfile.write(filename+',%s,%s,%s \n'% (x[j], x[k], std))
    	            csvfile.close()
    	else:
    	    std_bad.append(0) #If not, then the bad std. point is not stored and is given a passing value for that interval of 0
    	i=i+1
    filesave_std=(filename+'_std_checker.jpg') #filename to save image
    matplotlib.rcParams.update({'font.size':6}) #Alter font size
    std_checker=plt.figure() #Open pyplot figure
    plt.subplot(211) #Subplot of a 1x2 array
    plt.plot(interval, std_array) #Plot all standard deviations
    plt.xlabel('Interval of 200uV Input')
    plt.ylabel('Standard Dev. (counts)')
    plt.title('Plot of %s Chn:%s Standard Dev. along the Curve for 200uV Intervals'%(filenamechip, filenamechannel))
    plt.ylim(-15, 15)
    plt.subplot(212)
    plt.plot(interval, std_bad) #Plot all the bad standard deviations
    plt.ylim(-15, 15)
    plt.xlabel('Interval of 200uV Input')
    plt.ylabel('Standard Dev. (counts)')
    plt.title('Plot of Standard Dev. Above 5 Counts (All Other Intervals Given 0)')
    plt.savefig(filesave_std) #Save the image
    plt.close()    #Close pyplot
    filesave_std=(filename+'_std_checker_zoom.jpg')
    plt.plot(interval, std_array) #Plot all standard deviations
    plt.xlabel('Interval of 200uV Input')
    plt.ylabel('Standard Dev. (counts)')
    plt.title('Plot of %s Chn:%s Standard Dev. along the Curve for 200uV Intervals'%(filenamechip, filenamechannel))
    plt.ylim(-2, 2)
    plt.savefig(filesave_std) #Save the image
    plt.close()    #Close pyplot
    f=ROOT.TFile(filename+"_std.root", "recreate")
    t=ROOT.TTree("RMS", "RMS")
    rms=np.zeros(1, dtype=float)
    voltage=np.zeros(1, dtype=float)
    count=np.zeros(1, dtype=float)
    t.Branch('rms', rms, 'rms/D')
    t.Branch('voltage', voltage, 'voltage/D')
    t.Branch('count', count, 'count/D')
    i=0
    while i<len(interval):
        rms[0]=std_array[i]
	voltage[0]=interval[i]
    	count[0]=count_avg[i] 
	t.Fill()
	i=i+1
    f.Write()
    f.Close()       
def chi2_checker(filename, y, x, filenamechip, filenamechannel, directory): #Calculate approximate HWHM of 1k histogram points
    start, length=read_settings.get_linparameters(x, directory)
    x=x[start:length]
    y=y[start:length]    
    b=(len(y)/1000)-1  #Find the limit of the number of 1k point arrays
    i=0 #Variable for the loop to calculate all HWHM for the data file. 
    mean_array=[] #These are the arrays that we will plot and store
    chi_2_array=[]
    interval=[]
    j=0
    sigma=[]
    while i<b: #Loop to calculate the HWHM
    	j=1000*i #Indecies for the 1k data arrays
    	k=1000+j #End index for each 1k data array
    	#mean=np.mean(y[j:k]) #Caculate mean
    	interval.append(i) #Append as the interval of the 1k data array (x-axis)
    	#mean_array.append(mean) #Append as the mean of the interval (y-axis)
     	length=10
     	a=0
     	avg=[]
     	"""
     	while a<length:
     	    b=j+1000*a
     	    c=b+1000     	   
    	    m=np.mean(y[b:c])
            avg.append(m)
            a=a+1
        """
    	#hist, edges=np.histogram(avg, bins=10)
        hist, edges=np.histogram(y[j:k], bins=10)
        chi, p=stats.chisquare(hist)
        chi=chi/999.
        #sig=stats.norm
        chi_2_array.append(chi)
        i=i+1
    filesave_chi2=(filename+'_chi2_checker.jpg') #filename to save image
    matplotlib.rcParams.update({'font.size':6}) #Alter font size
    std_checker=plt.figure() #Open pyplot figure
    #plt.subplot(211) #Subplot of a 1x2 array
    plt.plot(interval, chi_2_array) #Plot all Chi2/DOF
    plt.ylim(0, 10)
    plt.yticks(np.arange(0, 10, 1.0))
    plt.xlabel('Interval of 1000 Points (200uV) Input')
    plt.ylabel('Chi2/DOF')
    plt.title('Plot of %s Chn:%s Chi_2 Value for 1000 Points (200uV) Intervals'%(filenamechip, filenamechannel))
    plt.savefig(filesave_chi2) #Save the image
    plt.close()    #Close pyplot
def linreg(filename, x, y, filenamechip, filenamechannel, directory): #Linear regression of BNL DUNE cold ADC data
    start, length=read_settings.get_linparameters(x, directory)
    #The data evens out at the end but we don't want a plateau to be included in the linear fit, so we cut it off around 6 million points in.
    slope, intercept, r_value, p_value, std_err=stats.linregress(x[start:length], y[start:length])
    r_squared=r_value**2 #The data files are way too large for scipy to calculate uncertainties, ROOT extensions will be added later
    r_squared=round(r_squared, 5) #For now, the r-squared value is a good indicator if we got a roughly correct fit. Value should be r^2=1(.0005)
    x_average_slice=[] #To calculate the residuals, in a way that makes sense, we take the average of a data point ad compare 
    y_average_slice=[]
    y_fit=[] #Arrays to calcuate the fit
    i=0
    diff=length-start #Amount of loops calculating the slice value
    b=(diff/1000)-1
    while i<b:
        m=start+i*1000
        n=np.mean(y[m:(1000+m)])
        o=x[m+500]
        x_average_slice.append(o)
        y_average_slice.append(n)
        i=i+1
    for i in x_average_slice:  
        fit =slope*i+intercept 
        y_fit.append(fit)
    i=0
    while i<(len(x_average_slice)-1):
        print (x_average_slice[i+1]-x_average_slice[i])/(x_average_slice[i]/y_fit[i])-1
        i=1+1
    y_residual=np.subtract(y_average_slice,y_fit) #Calculate a residual given the averages and the y_fit
    inl=x_average_slice[-1]/(341)/-y_average_slice[-1]
    filesave=(filename+'_linregress.jpg') #Create filename
    #filesave=(filename+'_linregress_zoom.jpg')
    matplotlib.rcParams.update({'font.size':6})
    linreg=plt.figure()
    plt.subplot(211) #Plot linear regression
    plt.plot(x[start:length], y[start:length])
    plt.plot(x_average_slice, y_fit)
    plt.xlabel('Input Voltage (uV)')
    if directory=='../data_17_1/data_repo_17_1/':
    	plt.xlabel('Input Voltage (500nV)')
    plt.ylabel('Output (Counts)')
    plt.title('Plot of %s Chn:%s ADC Counts with Linear Fit (R^2=%s) (m=%s) (b=%s)'%(filenamechip, filenamechannel, r_squared, slope, intercept))
    plt.subplot(212) #Plot residuals of the average
    plt.plot(x_average_slice, y_residual)
    plt.ylim(-20, 20)
    #plt.xlim(600000,610000)
    plt.xlabel('Input Voltage (uV)')
    if directory=='../data_17_1/data_repo_17_1/':
    	plt.xlabel('Input Voltage (500nV)')
    plt.ylabel('Residual (Counts)')
    plt.title('Residual Plots of Average Count Value for 200uV Intervals (avg_res=%s)'%(round(np.mean(y_residual),5)))
    plt.savefig(filesave) #Save plots
    filesave=(filename+'_linregress_zoom.jpg')
    plt.ylim(-1, 1)
    plt.savefig(filesave)
    plt.close() #Close pyplots
def hist_baseline(filename, x, y, baseline, filenamechip, filenamechannel): #Calculate the histograms near the baseline of the data
    i=0 
    interval=[]
    while i<15: #Create array of points near the baselines
        a=baseline+i*500
        interval.append(a)
        i=i+1
    hist_plots=plt.figure() #Create image
    sub=1
    hist_plots.suptitle('Histograms of %s Chn:%s ADC Near Baseline'%(filenamechip, filenamechannel), fontsize=12) #Title
    i=0 #Loops for the histograms
    j=0
    k=0
    while i<10:
        plt.subplot(5, 2, sub) #Histogram of every 100k points, create 10 plots
        plt.hist(y[interval[i]:interval[i+1]])
        std=np.std(y[interval[i]:interval[i+1]])
        #kurt=stats.kurtosis(y[interval[i]:interval[i+1]])
        plt.tight_layout(h_pad=.1)
        plt.ylabel("Frequency")
        plt.xlabel("ADC Output (Counts) (s=%s)"%(std))
        sub=sub+1
        i=i+1
    plt.subplots_adjust(top=.945)
    filesave=(filename+'_hist_base.jpg') #Save image
    plt.savefig(filesave)
    plt.close() #Close pyplot
def hist_avg_baseline(filename, x, y, baseline, filenamechip, filenamechannel):
    i=0 
    interval=[]
    while i<15: #Create array of points near the baselines
        a=baseline+i*100000
        interval.append(a)
        i=i+1
    hist_plots=plt.figure() #Create image
    sub=1
    hist_plots.suptitle('Histograms of Averaged 1k Point Data Samples of %s Chn:%s ADC Near Baseline'%(filenamechip, filenamechannel), fontsize=12) #Title
    i=0 #Loops for the histograms
    j=0
    k=0
    while i<10:
        plt.subplot(5, 2, sub) #Histogram of every 100k points using 1k averages
        a=0
        avg=[]
        st=[]
        while a<100:
            b=1000*a
            c=b+1000
            m=np.mean(y[interval[i]+b:interval[i]+c])
            s=np.std(y[interval[i]+b:interval[i]+c])
            st.append(s)
            avg.append(m)
            a=a+1
        plt.hist(avg, bins=10)
        hist, edges=np.histogram(avg, bins=10)
        chi, p=stats.chisquare(hist)
        plt.tight_layout(h_pad=.1)
        plt.ylabel("Frequency")
        plt.xlabel("ADC Output (Counts) (chi2=%s p=%s)"%(chi, p))
        sub=sub+1
        i=i+1
    plt.subplots_adjust(top=.945)
    filesave=(filename+'_hist_avg_base.jpg') #Save image
    plt.savefig(filesave)
    plt.close() #Close pyplot
def hist_random_baseline(filename, x, y, baseline, filenamechip, filenamechannel):
    i=0 
    interval=[baseline, baseline+10000]
    hist_plots=plt.figure() #Create image
    hist_plots.suptitle('Randomly Sampled 1k Point Histograms Data Samples of %s Chn:%s ADC Near Baseline'%(filenamechip, filenamechannel), fontsize=12) #Title
    i=0 #Loops for the histograms
    j=0
    k=0
    sub=1
    while i<10:
        plt.subplot(5, 2, sub) #Histogram of every 100k points using 1k averages
        a=0
        sample=[]
        while a<1000:
            b=1000*a
            c=b+1000
            m=rand.choice(y[interval[0]:interval[1]])
            sample.append(m)
            a=a+1
        plt.hist(sample, bins=10)
        #hist, edges=np.histogram(sample, bins=10)
        std=np.std(sample)
        mean=np.mean(sample)
        plt.tight_layout(h_pad=.1)
        plt.ylabel("Frequency")
        plt.xlabel("ADC Output (Counts) (m=%s s=%s)"%(mean, std))
        sub=sub+1
        i=i+1
    plt.subplots_adjust(top=.945)
    filesave=(filename+'_hist_rand_base.jpg') #Save image
    plt.savefig(filesave)
    plt.close() #Close pyplot
def gen_residual(x, y):
    start=5000000 
    #There are a lot of errors in the lower end of the data, this is a good place to start so we can get a good linear plot
    length=len(x)
    length=length-6000000 
    #The data evens out at the end but we don't want a plateau to be included in the linear fit, so we cut it off around 6 million points in.
    slope, intercept, r_value, p_value, std_err=stats.linregress(x[start:length], y[start:length])
    r_squared=r_value**2 #The data files are way too large for scipy to calculate uncertainties, ROOT extensions will be added later
    r_squared=round(r_squared, 5) #For now, the r-squared value is a good indicator if we got a roughly correct fit. Value should be r^2=1(.0005)
    x_average_slice=[] #To calculate the residuals, in a way that makes sense, we take the average of a data point ad compare 
    y_average_slice=[]
    y_fit=[] #Arrays to calcuate the fit
    i=0
    diff=length-start #Amount of loops calculating the slice value
    b=(diff/1000)-1
    while i<b:
        m=start+i*1000
        n=np.mean(y[m:(1000+m)])
        o=x[m+500]
        x_average_slice.append(o)
        y_average_slice.append(n)
        i=i+1
    for i in x_average_slice:  
        fit =slope*i+intercept 
        y_fit.append(fit)
    y_residual=np.subtract(y_average_slice,y_fit)
    residual_array=[x_average_slice, y_residual, slope, intercept]
    return residual_array
def residual_cali(filename, x, y, filenamechip, filenamechannel):
    start=5000000 
    #There are a lot of errors in the lower end of the data, this is a good place to start so we can get a good linear plot
    length=len(x)
    length=length-6000000 
    cali_filename_array=['P1_S7_02_LN_2MHz_chn0_01_19_14_39_sps','P1_S7_02_LN_2MHz_chn1_01_19_14_40_sps', 'P1_S7_02_LN_2MHz_chn2_01_19_14_40_sps',  'P1_S7_02_LN_2MHz_chn3_01_19_14_40_sps', 'P1_S7_02_LN_2MHz_chn4_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn5_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn6_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn7_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chn8_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chn9_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chnA_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnB_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnC_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnD_01_19_14_44_sps', 'P1_S7_02_LN_2MHz_chnE_01_19_14_44_sps', 'P1_S7_02_LN_2MHz_chnF_01_19_14_45_sps']
    channel=int(filenamechannel, 16)
    cali_filename=cali_filename_array[channel]
    cali_filename='../data_17_01/data_repo_17_01/'+cali_filename+'.csv'
    cali_data=csv_pull(cali_filename, directory)
    cali_residual=gen_residual(cali_data[0], cali_data[1])
    woo, woo2, slope_cur, intercept_curr=gen_residual(x, y)
    x_average_slice=[] #To calculate the residuals, in a way that makes sense, we take the average of a data point ad compare 
    y_average_slice=[]
    y_fit=[] #Arrays to calcuate the fit
    i=0
    diff=length-start #Amount of loops calculating the slice value
    b=(diff/1000)-1
    slope=cali_residual[2]
    intercept=cali_residual[3]
    while i<b:
        m=start+i*1000
        n=np.mean(y[m:(1000+m)])
        o=x[m+500]
        x_average_slice.append(o)
        y_average_slice.append(n)
        i=i+1
    for i in x_average_slice:  
        fit =slope*i+intercept 
        y_fit.append(fit)
    y_residual=np.subtract(y_average_slice,y_fit)
    curre=plt.plot(x_average_slice, y_residual, '-r', label='March Residuals')
    cali=plt.plot(cali_residual[0], cali_residual[1], '-b', label='Jan. Residuals')
    plt.legend(loc='upper left')
    plt.xlabel('Input Voltage (uV)')
    plt.ylabel('Residual Difference (Counts)')
    plt.title('Residuals of Jan. Data and March Data Using Jan. Lin Fit '+filenamechip+" Channel:"+filenamechannel+' (avg_resi_jan=%s, avg_resi_mar=%s)'%(round(np.mean(cali_residual[1]),5), round(np.mean(y_residual),5)))
    plt.ylim(-30, 30)
    filesave=(filename+'resi_diff.jpg') #Save image
    plt.savefig(filesave)
    plt.close()
    y_residual_adj=y_residual+(intercept-intercept_curr)
    curre=plt.plot(x_average_slice, y_residual_adj, '-r', label='Adj. March Residuals')
    cali=plt.plot(cali_residual[0], cali_residual[1], '-b', label='Jan. Residuals')
    plt.legend(loc='upper left')
    plt.xlabel('Input Voltage (uV)')
    plt.ylabel('Residual Difference (Counts)')
    plt.ylim(-30, 30)
    plt.title('Residuals Using Jan. Lin Fit with Adjustments  for Differences in y-intercepts '+filenamechip+" Channel:"+filenamechannel+' (avg_resi_jan=%s, avg_resi_adj_mar=%s)'%(round(np.mean(cali_residual[1]),5), round(np.mean(y_residual_adj),5)))
    filesave=(filename+'resi_diff_adj.jpg')
    plt.savefig(filesave)
    plt.close()
def linear_cali(x, y, filename, filenamechip, filenamechannel):
    start=5000000 
    #There are a lot of errors in the lower end of the data, this is a good place to start so we can get a good linear plot
    length=len(x)
    length=length-6000000 
    diff=length-start #Amount of loops calculating the slice value
    b=(diff/1000)-1
    cali_filename_array=['P1_S7_02_LN_2MHz_chn0_01_19_14_39_sps','P1_S7_02_LN_2MHz_chn1_01_19_14_40_sps', 'P1_S7_02_LN_2MHz_chn2_01_19_14_40_sps',  'P1_S7_02_LN_2MHz_chn3_01_19_14_40_sps', 'P1_S7_02_LN_2MHz_chn4_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn5_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn6_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn7_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chn8_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chn9_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chnA_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnB_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnC_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnD_01_19_14_44_sps', 'P1_S7_02_LN_2MHz_chnE_01_19_14_44_sps', 'P1_S7_02_LN_2MHz_chnF_01_19_14_45_sps']
    channel=int(filenamechannel, 16)
    cali_filename=cali_filename_array[channel]
    cali_filename='../data_17_01/data_repo_17_01/'+cali_filename+'.csv'
    cali_data=csv_pull(cali_filename, directory)
    slope, intercept, r_value, p_value, std_err=stats.linregress(cali_data[0][start:length], cali_data[1][start:length])
    x_average_slice=[] #To calculate the residuals, in a way that makes sense, we take the average of a data point ad compare 
    y_average_slice=[]
    y_fit=[] #Arrays to calcuate the fit
    print slope, intercept
    i=0
    while i<b:
        m=start+i*1000
        n=np.mean(y[m:(1000+m)])
        o=x[m+500]
        x_average_slice.append(o)
        y_average_slice.append(n)
        i=i+1
    y_fit=[j*slope+intercept for j in x_average_slice]
    print y_fit[:10]
    #print y_fit
    y_residual=np.subtract(y_average_slice,y_fit)
    print y_residual[:10]
    plt.plot(x_average_slice, y_residual, '-r', label='Residuals for Mar. using Jan. Linear Fit')
    plt.legend(loc='upper left')
    plt.xlabel('Input Voltage (uV)')
    plt.ylabel('Residual (Counts)')
    plt.title('Residuals Between January Linear Fit and March Dataset for '+filenamechip+" Channel:"+filenamechannel+' (slope=%s, y-intercept=%s, avg_resi=%s)'%(round(slope,11), round(intercept,5),round(np.mean(y_residual), 5)))
    plt.ylim(-300, 300)
    filesave=(filename+'lin_diff.jpg') #Save image
    plt.savefig(filesave)
    plt.close()
    plt.plot(x_average_slice, y_residual, '-r', label='Residuals for Mar. using Jan. Linear Fit')
    plt.legend(loc='upper left')
    plt.xlabel('Input Voltage (uV)')
    plt.ylabel('Residual (Counts)')
    plt.title('Residuals of January Lin. Fit and March Data for '+filenamechip+" Channel:"+filenamechannel+' (slope=%s, y-intercept=%s, avg_resi=%s)'%(round(slope,11), round(intercept,5),round(np.mean(y_residual), 5)))
    plt.ylim(-40, 40)
    filesave=(filename+'lin_diff_zoom.jpg') #Save image
    plt.savefig(filesave)
    plt.close()
def scatter(filename, x, y, filenamechip, filenamechannel): #Create general scatter plot of data
    plt.plot(x, y) #Plot the data
    filesave=(filename+'_scatter.jpg')
    plt.xlabel('Input Voltage (uV)')
    plt.ylabel('Output (Counts)')
    plt.title('Scatterplot of %s Chn:%s '%(filenamechip, filenamechannel))
    plt.savefig(filesave) #Save the image
    plt.close() #Close pyplot
def histogram_dynamic(filename, x, y, filenamechip, filenamechannel, directory):
    length=len(x)
    i=0
    h=len(x)-2000000
    v_max=1400000
    bit_max=4095
    while i<len(x):
        if y[i]==3686:
            maximum=i
            h=maximum
            #print y[i] 
            v_max=i*.1
            bit_max=y[1]
            i=len(x)*1.
        i=i+1
    hist_mean=[]
    hist_std=[]
    vol_mean=[]
    vol_std=[]
    dnl=[]
    inl=[]
    j=0
    diff=3450
    i=h
    max_bit=3276
    if directory=='../data_cots/data_repo_cots/' or directory=='../data_17_06/data_repo_17_06/':
        diff=diff/2
    elif directory=='../data_17_07/data_repo_17_07/':
        diff=int(diff/2)
    while j<max_bit:
        vol=np.mean(x[(i-diff):i])
        count=np.mean(y[(i-diff):i])
        std=np.std(y[(i-diff):i])
        vol_s=np.std(x[(i-diff):i])
        hist_mean.append(count)
        hist_std.append(std)
        vol_mean.append(vol)
        vol_std.append(vol_s)
        i=i-diff
        j=j+1
    i=0
    #dnl_tot= (vol_mean[0]-vol_mean[-1])/((v_max/bit_max)*(hist_mean[0]-hist_mean[-1]+.0000000001))-1
    #print dnl_tot
    #inl_tot=((vol_mean[0])/(v_max/bit_max))-hist_mean[0]
    #print inl_tot
    while i<(len(vol_mean)-1):
        #dnl_calc=((vol_mean[i]-vol_mean[i+1])/(np.absolute((hist_mean[i]-hist_mean[i+1]+.000001))*v_max/bit_max))-1. 
        #dnl.append(dnl_calc)
        #inl_calc=((vol_mean[i])/(v_max/bit_max))-hist_mean[i]
        #print inl_calc
        #inl.append(inl_calc)
        i=i+1
    return [vol_mean, hist_mean, vol_std, hist_std, dnl, inl]
def histreg(filename, x, y, filenamechip, filenamechannel, directory): #Linear regression of BNL DUNE cold ADC data
    #There are a lot of errors in the lower end of the data, this is a good place to start so we can get a good linear plot
    length=len(x)
    #The data evens out at the end but we don't want a plateau to be included in the linear fit, so we cut it off around 6 million points in.
    slope, intercept, r_value, p_value, std_err=stats.linregress(x, y)
    r_squared=r_value**2 #The data files are way too large for scipy to calculate uncertainties, ROOT extensions will be added later
    r_squared=round(r_squared, 5) #For now, the r-squared value is a good indicator if we got a roughly correct fit. Value should be r^2=1(.0005)
    y_fit=[] #Arrays to calcuate the fit
    i=0
    b=len(x)
    for i in x:  
        fit =slope*i+intercept 
        y_fit.append(fit)
    y_residual=np.subtract(y,y_fit) #Calculate a residual given the averages and the y_fit
    filesave=(filename+'_histregress.jpg') #Create filename
    #filesave=(filename+'_linregress_zoom.jpg')
    matplotlib.rcParams.update({'font.size':6})
    linreg=plt.figure()
    plt.subplot(211) #Plot linear regression
    plt.plot(x, y)
    plt.plot(x, y)
    plt.xlabel('Input Voltage (uV)')
    if directory=='../data_17_1/data_repo_17_1/':
    	plt.xlabel('Input Voltage (500nV)')
    plt.ylabel('Output (Counts)')
    plt.title('Plot of %s Chn:%s ADC Counts with Linear Fit (R^2=%s) (m=%s) (b=%s)'%(filenamechip, filenamechannel, r_squared, slope, intercept))
    plt.subplot(212) #Plot residuals of the average
    plt.plot(x, y_residual)
    plt.ylim(-20, 20)
    #plt.xlim(600000,610000)
    plt.xlabel('Input Voltage (uV)')
    if directory=='../data_17_1/data_repo_17_1/':
    	plt.xlabel('Input Voltage (500nV)')
    plt.ylabel('Residual (Counts)')
    plt.title('Residual Plots of Average Count Value for 200uV Intervals (avg_res=%s)'%(round(np.mean(y_residual),5)))
    plt.savefig(filesave) #Save plots
    plt.close() #Close pyplots   
    return [x, y_residual, slope, intercept] 
def histreg_inv(filename, x, y, filenamechip, filenamechannel, directory): #Linear regression of BNL DUNE cold ADC data
    #There are a lot of errors in the lower end of the data, this is a good place to start so we can get a good linear plot
    length=len(x)
    #The data evens out at the end but we don't want a plateau to be included in the linear fit, so we cut it off around 6 million points in.
    slope, intercept, r_value, p_value, std_err=stats.linregress(y, x)
    r_squared=r_value**2 #The data files are way too large for scipy to calculate uncertainties, ROOT extensions will be added later
    r_squared=round(r_squared, 5) #For now, the r-squared value is a good indicator if we got a roughly correct fit. Value should be r^2=1(.0005)
    x_fit=[] #Arrays to calcuate the fit
    i=0
    b=len(x)
    for i in y:  
        fit =slope*i+intercept 
        x_fit.append(fit)
    x_residual=np.subtract(x,x_fit) #Calculate a residual given the averages and the y_fit
    filesave=(filename+'_histregress_inv.jpg') #Create filename
    #filesave=(filename+'_linregress_zoom.jpg')
    matplotlib.rcParams.update({'font.size':6})
    linreg=plt.figure()
    plt.subplot(211) #Plot linear regression
    plt.plot(y, x)
    plt.ylabel('Input Voltage (uV)')
    if directory=='../data_17_1/data_repo_17_1/':
    	plt.xlabel('Input Voltage (500nV)')
    plt.xlabel('Output (Counts)')
    plt.title('Plot of %s Chn:%s ADC Counts with Linear Fit (R^2=%s) (m=%s) (b=%s)'%(filenamechip, filenamechannel, r_squared, slope, intercept))
    plt.subplot(212) #Plot residuals of the average
    plt.plot(y, x_residual)
    plt.ylim(-2500, 2500)
    #plt.xlim(600000,610000)
    plt.ylabel('Residual Voltage (uV)')
    if directory=='../data_17_1/data_repo_17_1/':
    	plt.xlabel('Input Voltage (500nV)')
    plt.xlabel('Output (Counts)')
    plt.title('Residual Plots of Average Count Value for 200uV Intervals (avg_res=%s)'%(round(np.mean(x_residual),5)))
    plt.savefig(filesave) #Save plots
    plt.ylim(-1000, 1000)
    filesave=(filename+'_histregress_inv_zoom.jpg')
    plt.close() #Close pyplots   
    return [y, x_residual, slope, intercept] 
def hist_resi(filename, x, y, filenamechip, filenamechannel, directory):
    #There are a lot of errors in the lower end of the data, this is a good place to start so we can get a good linear plot
    length=len(x)
    cali_filename_array=['P1_S7_02_LN_2MHz_chn0_01_19_14_39_sps','P1_S7_02_LN_2MHz_chn1_01_19_14_40_sps', 'P1_S7_02_LN_2MHz_chn2_01_19_14_40_sps',  'P1_S7_02_LN_2MHz_chn3_01_19_14_40_sps', 'P1_S7_02_LN_2MHz_chn4_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn5_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn6_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn7_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chn8_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chn9_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chnA_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnB_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnC_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnD_01_19_14_44_sps', 'P1_S7_02_LN_2MHz_chnE_01_19_14_44_sps', 'P1_S7_02_LN_2MHz_chnF_01_19_14_45_sps']
    channel=int(filenamechannel, 16)
    cali_filename=cali_filename_array[channel]
    cali_filename='../data_17_01/data_repo_17_01/'+cali_filename+'.csv'
    data=np.array(csv_pull(cali_filename, directory), dtype=object) 
    hists=histogram_dynamic(cali_filename, data[0], data[1], filenamechip, filenamechannel, directory)
    cali_residual=histreg(cali_filename, hists[0], hists[1], filenamechip, filenamechannel, directory)
    curr_residual=histreg(filename, x, y, filenamechip, filenamechannel, directory)
    y_fit=[] #Arrays to calcuate the fit
    slope=cali_residual[2]
    intercept=cali_residual[3]
    for i in x:  
        fit =slope*i+intercept 
        y_fit.append(fit)
    y_residual=np.subtract(y,y_fit)
    curre=plt.plot(x, y_residual, '-r', label='March Residuals')
    cali=plt.plot(cali_residual[0], cali_residual[1], '-b', label='Jan. Residuals')
    plt.legend(loc='upper left')
    plt.xlabel('Input Voltage (uV)')
    plt.ylabel('Residual Difference (Counts)')
    plt.title('Residuals of Jan. Data and March Data Using Jan. Lin Fit '+filenamechip+" Channel:"+filenamechannel+' (avg_resi_jan=%s, avg_resi_mar=%s)'%(round(np.mean(cali_residual[1]),5), round(np.mean(y_residual),5)))
    plt.ylim(-30, 30)
    filesave=(filename+'resi_hist.jpg') #Save image
    plt.savefig(filesave)
    plt.close()
    intercept_curr=curr_residual[3]
    y_residual_adj=y_residual+(intercept-intercept_curr)
    curre=plt.plot(x, y_residual_adj, '-r', label='Adj. March Residuals')
    cali=plt.plot(cali_residual[0], cali_residual[1], '-b', label='Jan. Residuals')
    plt.legend(loc='upper left')
    plt.xlabel('Input Voltage (uV)')
    plt.ylabel('Residual Difference (Counts)')
    plt.ylim(-30, 30)
    plt.title('Residuals Using Jan. Lin Fit with Adjustments  for Differences in y-intercepts '+filenamechip+" Channel:"+filenamechannel+' (avg_resi_jan=%s, avg_resi_adj_mar=%s)'%(round(np.mean(cali_residual[1]),5), round(np.mean(y_residual_adj),5)))
    filesave=(filename+'resi_hist_adj.jpg')
    plt.savefig(filesave)
    plt.close()  
def hist_resi_inv(filename, x, y, filenamechip, filenamechannel, directory):
    #There are a lot of errors in the lower end of the data, this is a good place to start so we can get a good linear plot
    length=len(x)
    cali_filename_array=['P1_S7_02_LN_2MHz_chn0_01_19_14_39_sps','P1_S7_02_LN_2MHz_chn1_01_19_14_40_sps', 'P1_S7_02_LN_2MHz_chn2_01_19_14_40_sps',  'P1_S7_02_LN_2MHz_chn3_01_19_14_40_sps', 'P1_S7_02_LN_2MHz_chn4_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn5_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn6_01_19_14_41_sps', 'P1_S7_02_LN_2MHz_chn7_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chn8_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chn9_01_19_14_42_sps', 'P1_S7_02_LN_2MHz_chnA_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnB_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnC_01_19_14_43_sps', 'P1_S7_02_LN_2MHz_chnD_01_19_14_44_sps', 'P1_S7_02_LN_2MHz_chnE_01_19_14_44_sps', 'P1_S7_02_LN_2MHz_chnF_01_19_14_45_sps']
    channel=int(filenamechannel, 16)
    cali_filename=cali_filename_array[channel]
    cali_filename='../data_17_01/data_repo_17_01/'+cali_filename+'.csv'
    data=np.array(csv_pull(cali_filename, directory), dtype=object) 
    hists=histogram_dynamic(cali_filename, data[0], data[1], filenamechip, filenamechannel, directory)
    cali_residual=histreg(cali_filename, hists[0], hists[1], filenamechip, filenamechannel, directory)
    curr_residual=histreg(filename, x, y, filenamechip, filenamechannel, directory)
    y_fit=[] #Arrays to calcuate the fit
    slope=cali_residual[2]
    intercept=cali_residual[3]
    for i in x:  
        fit =slope*i+intercept 
        y_fit.append(fit)
    y_residual=np.subtract(y,y_fit)
    curre=plt.plot(x, y_residual, '-r', label='March Residuals')
    cali=plt.plot(cali_residual[0], cali_residual[1], '-b', label='Jan. Residuals')
    plt.legend(loc='upper left')
    plt.xlabel('Input Voltage (uV)')
    plt.ylabel('Residual Difference (Counts)')
    plt.title('Residuals of Jan. Data and March Data Using Jan. Lin Fit '+filenamechip+" Channel:"+filenamechannel+' (avg_resi_jan=%s, avg_resi_mar=%s)'%(round(np.mean(cali_residual[1]),5), round(np.mean(y_residual),5)))
    plt.ylim(-30, 30)
    filesave=(filename+'resi_hist.jpg') #Save image
    plt.savefig(filesave)
    plt.close()
    intercept_curr=curr_residual[3]
    y_residual_adj=y_residual+(intercept-intercept_curr)
    curre=plt.plot(x, y_residual_adj, '-r', label='Adj. March Residuals')
    cali=plt.plot(cali_residual[0], cali_residual[1], '-b', label='Jan. Residuals')
    plt.legend(loc='upper left')
    plt.xlabel('Input Voltage (uV)')
    plt.ylabel('Residual Difference (Counts)')
    plt.ylim(-30, 30)
    plt.title('Residuals Using Jan. Lin Fit with Adjustments  for Differences in y-intercepts '+filenamechip+" Channel:"+filenamechannel+' (avg_resi_jan=%s, avg_resi_adj_mar=%s)'%(round(np.mean(cali_residual[1]),5), round(np.mean(y_residual_adj),5)))
    filesave=(filename+'resi_hist_adj.jpg')
    plt.savefig(filesave)
    plt.close()    
def calc_inl_dnl(filename, x, y, filenamechip, filenamechannel, directory):
    #There are a lot of errors in the lower end of the data, this is a good place to start so we can get a good linear plot
    length=len(x)
    #The data evens out at the end but we don't want a plateau to be included in the linear fit, so we cut it off around 6 million points in.
    slope, intercept, r_value, p_value, std_err=stats.linregress(y, x)
    slope_bity, intercept_bity, r_value, p_value, std_err=stats.linregress(x, y)
    r_squared=r_value**2 #The data files are way too large for scipy to calculate uncertainties, ROOT extensions will be added later
    r_squared=round(r_squared, 5) #For now, the r-squared value is a good indicator if we got a roughly correct fit. Value should be r^2=1(.0005)
    x_fit=[] #Arrays to calcuate the fit
    i=0
    b=len(x)
    for i in y:  
        fit =slope*i+intercept 
        x_fit.append(fit)
    x_cali=[i-intercept for i in x]
    slope, intercept, r_value, p_value, std_err=stats.linregress(y,x)
    x_cali_fit=[]
    for i in y:
        fit=slope*i+intercept
        x_cali_fit.append(fit)	
    f=ROOT.TFile(filename+"_DNL_INL.root", "recreate")
    t=ROOT.TTree("DNL_INL", "DNL_INL")
    dnl=np.zeros(1, dtype=float)
    voltage=np.zeros(1, dtype=float)
    count=np.zeros(1, dtype=float)
    inl_end=np.zeros(1, dtype=float)
    inl=np.zeros(1, dtype=float)
    t.Branch('dnl', dnl, 'dnl/D')
    t.Branch('inl', inl, 'inl/D')
    t.Branch('inl_end', inl_end, 'inl_end/D')
    t.Branch('voltage', voltage, 'voltage/D')
    t.Branch('count', count, 'count/D')
    i=0
    while i<(len(x)-1):
        dnl[0]=(x_cali[i]-x_cali[i+1])/((y[i]-y[i+1]+.0001)*((1)/slope_bity))-1.
        #print dnl
        inl[0]=(x_cali[i]-intercept)/((1)/slope_bity)-y[i]
	voltage[0]=x[i]
        count[0]=y[i]
        if i==0:
            inl_end[0]=(x_cali[i]-intercept)/((1)/slope_bity)-y[i]
	t.Fill()
	i=i+1
    t.Fill()
    f.Write()
    f.Close()
   
    
    
    
         
