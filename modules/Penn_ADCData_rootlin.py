from ROOT import TCanvas, TGraphErrors, TF1, TGraph
from ROOT import gROOT
import ROOT
from array import array
import numpy as np
import DUNE_ADCSettings as read_settings
def lin_fit(filename, x, y, filenamechip, filenamechannel, directory):
    start, length=read_settings.get_linparameters(x, directory)
    x=x[start:length]
    y=y[start:length]
    ROOT.gStyle.SetOptStat() # shows the mean, rms, and entries
    ROOT.gStyle.SetOptFit()
    ROOT.gROOT.SetBatch(True)
    c1 = TCanvas( 'c1', filename , 200, 10, 700, 500 )
    fit=TF1('linear',"pol0(0)+pol0(1)*x",x[0],len(x));
    fit.SetParNames("Offset (counts)", "Slope (count/uV)");
    c1.SetFillColor( 0)
    c1.SetGrid()
    c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )
    length=len(x)
    ex=[0]*len(x)
    ey=[3.5]*len(x)
    if directory=='../data_cots/data_repo_cots/':
        ey=[1]*len(x)
    n = length;
    x  = array( 'f', x )
    ex = array( 'f', ex )
    y  = array( 'f', y )
    ey = array( 'f', ey)
    gr = TGraphErrors( n, x, y, ex, ey)
    ge=c1.GetListOfPrimitives().FindObject("Graph")
    title=("Linear Regression of Cold ADC Output "+filenamechip+" Chn: "+filenamechannel)
    gr.SetTitle(title)
    gr.SetMarkerColor( 4 )
    gr.GetXaxis().SetRangeUser(4500000, 14000000)
    gr.GetXaxis().SetTitle("ADC Input (uV)")
    gr.GetYaxis().SetTitle("ADC Output (Counts)")
    gr.SetMarkerStyle( 21 )
    #st=ge.GetListOfFunctions().FindObject('stats')
    #st.SetX1NDC(4500000)
    #st.SetX2NDC(6000000)
    pic_file=filename+"_root_linreg.png"
    gr.Fit('linear')
    gr.Draw( "ap" )
    #c1.SetBatch(kTRUE);
    c1.Modified()
    #c1.Update()
    c1.Print(pic_file)
    #stay open until 1 is entered
def linfit_hist(filename, x, y, ex, ey, filenamechip, filenamechannel, directory):
    ROOT.gStyle.SetOptStat() # shows the mean, rms, and entries
    ROOT.gStyle.SetOptFit()
    ROOT.gROOT.SetBatch(True)
    c1 = TCanvas( 'c1', filename , 200, 10, 700, 500 )
    fit=TF1('linear',"pol0(0)+pol0(1)*x",x[0],len(x));
    fit.SetParNames("Offset (counts)", "Slope (count/uV)");
    c1.SetFillColor( 0)
    c1.SetGrid()
    c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )
    length=len(x)
    n = length;
    x  = array( 'f', x )
    ex = array( 'f', ex )
    y  = array( 'f', y )
    ey = array( 'f', ey)
    gr = TGraphErrors( n, x, y, ex, ey)
    ge=c1.GetListOfPrimitives().FindObject("Graph")
    title=("Regression of Cold ADC Output "+filenamechip+" Chn: "+filenamechannel)
    gr.SetTitle(title)
    gr.SetMarkerColor( 4 )
    gr.GetXaxis().SetRangeUser(4500000, 14000000)
    gr.GetXaxis().SetTitle("ADC Input (uV)")
    gr.GetYaxis().SetTitle("ADC Output (Counts)")
    gr.SetMarkerStyle( 21 )
    #st=ge.GetListOfFunctions().FindObject('stats')
    #st.SetX1NDC(4500000)
    #st.SetX2NDC(6000000)
    pic_file=filename+"_root_histreg.png"
    gr.Fit('linear', "Q")
    gr.Draw( "ap" )
    #c1.SetBatch(kTRUE);
    c1.Modified()
    #c1.Update()
    c1.Print(pic_file)
    f=ROOT.TFile(filename+"_linfit.root", "recreate")
    t=ROOT.TTree("linfit", "linfit")
    slope=np.zeros(1, dtype=float)
    y_intercept=np.zeros(1, dtype=float)
    chi2=np.zeros(1, dtype=float)
    dof=np.zeros(1, dtype=float)
    chi2_dof=np.zeros(1, dtype=float)
    slope_err=np.zeros(1, dtype=float)
    y_intercept_err=np.zeros(1, dtype=float)
    t.Branch('slope', slope, 'slope/D')
    t.Branch('y_intercept', y_intercept, 'y_intercept/D')
    t.Branch('chi2', chi2, 'chi2/D')
    t.Branch('dof', dof, 'dof/D')
    t.Branch('chi2_dof', chi2_dof, 'chi2_dof/D')
    t.Branch('slope_err', slope_err, 'slope_err/D')
    t.Branch('y_intercpet_err', y_intercept_err, 'y_intercept_err/D')
    slope[0]=gr.GetFunction('linear').GetParameter(1)
    slope_err[0]=gr.GetFunction('linear').GetParError(1)
    y_intercept[0]=gr.GetFunction('linear').GetParameter(0)
    y_intercept_err[0]=gr.GetFunction('linear').GetParError(0)
    chi2[0]=gr.GetFunction('linear').GetChisquare()
    dof[0]=gr.GetFunction('linear').GetNDF()
    chi2_dof[0]=chi2[0]/dof[0]
    t.Fill()
    t.Fill()
    f.Write()
    f.Close()
    #stay open until 1 is entered        
def linfit_hist_inv(filename, x, y, ex, ey, filenamechip, filenamechannel, directory):
    ROOT.gStyle.SetOptStat() # shows the mean, rms, and entries
    ROOT.gStyle.SetOptFit()
    ROOT.gROOT.SetBatch(True)
    c1 = TCanvas( 'c1', filename , 200, 10, 700, 500 )
    fit=TF1('linear',"pol0(0)+pol0(1)*x",x[0],len(x));
    fit.SetParNames("Offset (uV)", "Slope (uV/counts)");
    c1.SetFillColor( 0)
    c1.SetGrid()
    c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )
    length=len(x)
    n = length;
    x  = array( 'f', x )
    ex = array( 'f', ex )
    y  = array( 'f', y )
    ey = array( 'f', ey)
    gr = TGraphErrors( n, y, x, ey, ex)
    ge=c1.GetListOfPrimitives().FindObject("Graph")
    title=("Regression of ADC Output "+filenamechip+" Chn: "+filenamechannel)
    gr.SetTitle(title)
    gr.SetMarkerColor( 4 )
    #gr.GetYaxis().SetRangeUser(4500000, 14000000)
    gr.GetYaxis().SetTitle("ADC Input (uV)")
    gr.GetXaxis().SetTitle("ADC Output (Counts)")
    gr.SetMarkerStyle( 21 )
    #st=ge.GetListOfFunctions().FindObject('stats')
    #st.SetX1NDC(4500000)
    #st.SetX2NDC(6000000)
    pic_file=filename+"_root_histreg_inv.png"
    gr.Fit('linear')
    gr.Draw( "ap" )
    #c1.SetBatch(kTRUE);
    c1.Modified()
    #c1.Update()
    c1.Print(pic_file)
    #stay open until 1 is entered
    f=ROOT.TFile(filename+"_INL.root", "recreate")
    t=ROOT.TTree("INL", "INL")
    inl=np.zeros(len(x), dtype=float)
    t.Branch('inl', inl, 'inl/D')
    for i in range(0, len(x)):
        inl[i]=x[i]
        t.Fill()
    f.Write()
    f.Close()
